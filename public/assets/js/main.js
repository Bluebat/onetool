$(function() {
    $('#email_error').hide();
    $('#form1').submit(function (e) {
        var val = $("#email").val();
        var endpoint = "https://open.kickbox.com/v1/disposable/";

                $.get({
                    url: endpoint + encodeURIComponent(val)
                }).done(function (data) {
                    if (data.disposable !== false) {
                        $('#email_error').show();
                        e.preventDefault();
                    }
                }).fail(function (data) {
                    $('#email_error').show();
                    e.preventDefault();
                });
    });
});