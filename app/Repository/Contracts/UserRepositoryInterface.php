<?php

declare(strict_types=1);

namespace App\Repository\Contracts;

interface UserRepositoryInterface
{
	/**
	 * @param string $name
	 * @param string $email
	 * @throws \ReflectionException
	 */
	public function save(string $name, string $email): void;

	/**
	 * @return array
	 */
	public function all(): array;
}
