<?php

declare(strict_types=1);

namespace App\Repository;

use App\Models\User;
use App\Repository\Contracts\UserRepositoryInterface;

final class UserRepository implements UserRepositoryInterface
{
	/**
	 * @var User
	 */
	private $user;

	public function __construct(User $user)
	{
		$this->user = $user;
	}

	/**
	 * @param string $name
	 * @param string $email
	 * @throws \ReflectionException
	 */
	public function save(string $name, string $email): void
	{
		$this->user->save([
			'name' => $name,
			'email' => $email,
		]);
	}

	/**
	 * @return array
	 */
	public function all(): array
	{
		return $this->user->findAll();
	}
}
