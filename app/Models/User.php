<?php

declare(strict_types=1);

namespace App\Models;

use CodeIgniter\Model;

class User extends Model
{
	protected $table = 'users';
	protected $allowedFields = ['name', 'email'];
}
