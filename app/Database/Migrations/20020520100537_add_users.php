<?php

declare(strict_types=1);

namespace App\Database\Migrations;

final class AddUsersTable extends \CodeIgniter\Database\Migration {

	public function up()
	{
		$this->forge->addField([
			'id'          => [
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => TRUE,
				'auto_increment' => TRUE
			],
			'name'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '200',
			],
			'email' => [
				'type'           => 'VARCHAR',
				'constraint'     => '200',
			],
		]);
		$this->forge->addKey('id', TRUE);
		$this->forge->createTable('users');
	}

	public function down()
	{
		$this->forge->dropTable('users');
	}
}
