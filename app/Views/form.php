<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Register</title>
	<meta name="description" content="The small framework with powerful features">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css"
		  href="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.6.3/css/foundation.min.css">
</head>
	<body>
		<div style="margin-top: 10%;"> &nbsp; </div>
		<div class="grid-x medium-grid-frame">
			<div class="cell  small-3">
				&nbsp;
			</div>
			<div class="cell  small-3">
				<form method="post" action="/home/register" id="form1">
					<label>E-mail:</label>
					<input type="text" placeholder="Type your name" id="email" name="name" />
					<input type="text" placeholder="Type your e-mail" id="email" name="email" />
					<p id="email_error">Disposable email addresses detected!</p>
					<button type="submit" class="button">Register</button>
				</form>
				<hr>
				<h2>Registred users:</h2>
				<?php
				if (!empty($users)) {
					foreach ($users as $user) {
						echo $user['name'] . ' ' . $user['email'] . '<br>';
					}
				}
				?>
			</div>
			<div class="cell  small-6">
				&nbsp;
			</div>
		</div>
		<script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"
			integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="></script>
		<script src="https://www.google.com/recaptcha/api.js?render=6LcmbfsUAAAAAJ7Dro4BZgBq4palsdWNK59B12By"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.6.3/js/foundation.min.js"></script>
		<script src="assets/js/main.js"></script>
	</body>
</html>
