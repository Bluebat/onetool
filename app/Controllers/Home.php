<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Models\User;
use App\Repository\UserRepository;

final class Home extends BaseController
{
	/**
	 * @var UserRepository
	 */
	private $userRepository;

	/**
	 * should be use Dependency Container but it do not exist in codeingniter:
	 * public function __construct(UserRepositoryInterface $userRepository)
	 */
	public function __construct()
	{
		$this->userRepository = new UserRepository(new User());
	}

	/**
	 * @return string
	 */
	public function index(): string
	{
		return view('form.php', ['users' => $this->userRepository->all()]);
	}

	/**
	 * @return string
	 * @throws \ReflectionException
	 */
	public function register(): string
	{
		$email = $this->request->getVar('email');
		$name = $this->request->getVar('name');
		if (empty($email) || empty($name)) {
			return view('fail.php');
		}
		$this->userRepository->save($name, $email);
		return view('success.php');
	}
}
